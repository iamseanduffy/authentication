﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeanDuffy.Authentication.Common;

namespace SeanDuffy.Authentication.Client
{
    public class Configuration : IConfiguration
    {
        public string ResourceName
        {
            get { return ConfigurationManager.AppSettings[Constants.CONFIG_RESOURCEID]; }
        }

        public string TimeOut
        {
            get { return ConfigurationManager.AppSettings[Constants.CONFIG_TIMEOUT_INSECS]; }
        }

        public string ServerEndpoint
        {
            get { return ConfigurationManager.AppSettings[Constants.CONFIG_SERVERENDPOINT]; }
        }
    }
}
