﻿using SeanDuffy.Authentication.Common;

namespace SeanDuffy.Authentication.Client
{
    public interface IApiClient
    {
        ValidationResponse RequestAuthorization(AuthRequest authRequest);
    }
}