﻿
using SeanDuffy.Authentication.Client;
using SeanDuffy.Authentication.Common;

namespace Client.Tests.Fakes
{
    public class FullMockApiClient : IApiClient
    {
        public bool IsRequestAuthorizationCalled { get; private set; }
        public AuthRequest AuthorizationRequestCreated { get; private set; }
        public bool ReturnSuccesfulAuthentication { private get; set; }
        public ValidationResponse RequestAuthorization(AuthRequest authRequest)
        {
            AuthorizationRequestCreated = authRequest;

            IsRequestAuthorizationCalled = true;
            return ReturnSuccesfulAuthentication
                ? new ValidationResponse { IsPasswordCorrect = true, IsUserIdentifierFound = true, IsUserUnlocked = true }
                : new ValidationResponse();
        }
    }
}
