﻿using System.ComponentModel.Composition;

namespace SeanDuffy.Authentication.Services.Hashing
{
    [InheritedExport]
    public interface IHasher
    {
        string GetHash(string value, string salt);
    }
}