﻿using SeanDuffy.Authentication.Common;
using SeanDuffy.Authentication.Domain;

namespace SeanDuffy.Authentication.Services.Login
{
    public class LoginContext
    {
        public Identity StoredIdentity { get; set; }

        public AuthRequest Request { get; set; }
    }
}